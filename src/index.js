require('module-alias/register');

const boardFuncs = require('@local/board');
const formatFuncs = require('@local/format');

class ChessClock {
  constructor(input = null) {
    this.raw = {
      boardFuncs: boardFuncs,
      formatFuncs: formatFuncs
    };
    this.clock = {
      running: false,
      lastUpdate: -1,
      player: 'white',
      whiteDurationLeft: 0,
      blackDurationLeft: 0,
      whiteDelayLeft: 0,
      blackDelayLeft: 0,
      startingDuration: 0,
      flatIncrement: 0,
      presentIncrement: 0,
      activeIncrement: 0,
      timelineIncrement: 0,
      flatDelay: 0,
      presentDelay: 0,
      activeDelay: 0,
      timelineDelay: 0,
    };
    if(input !== null) {
      this.import(input);
    }
  }
  import(input) {
    if(typeof input === 'object') {
      Object.assign(this.clock, input);
    }
    try {
      let object = JSON.parse(input);
      Object.assign(this.clock, object);
      this.reset();
    }
    catch(err) {
      this.reset(input);
    }
  }
  copy(time = null) {
    let newInstance = new ChessClock();
    newInstance.state(this.state(), time);
    return newInstance;
  }
  state(state = null, time = null) {
    this.updateTime(time);
    if(state === null) {
      return Object.assign({}, this.clock);
    }
    else {
      Object.assign(this.clock, state);
    }
  }
  reset(input = null) {
    if(typeof input === 'string') {
      Object.assign(this.clock, formatFuncs.fromString(input));
    }
    this.clock.running = false;
    this.clock.lastUpdate = -1;
    this.clock.player = 'white';
    this.clock.whiteDurationLeft = this.clock.startingDuration;
    this.clock.blackDurationLeft = this.clock.startingDuration;
  }
  start(board = {}, time = null) {
    if(!this.clock.running) {
      //Initial start
      if(this.clock.lastUpdate < 0) {
        this.addTime(board);
      }
      this.clock.running = true;
      this.clock.lastUpdate = Date.now();
      if(typeof time === 'number') {
        this.clock.lastUpdate = time;
      }
    }
  }
  stop(time = null) {
    this.updateTime(time);
    if(this.clock.running) {
      this.clock.running = false;
    }
  }
  addTime(board = {}) {
    let timelineInfo = boardFuncs.parse(board);
    this.clock[`${this.clock.player}DurationLeft`] += this.clock.flatIncrement;
    this.clock[`${this.clock.player}DurationLeft`] += this.clock.presentIncrement * timelineInfo.presentTimelines;
    this.clock[`${this.clock.player}DurationLeft`] += this.clock.activeIncrement * timelineInfo.activeTimelines;
    this.clock[`${this.clock.player}DurationLeft`] += this.clock.timelineIncrement * timelineInfo.timelines;
    this.clock[`${this.clock.player}DelayLeft`] = this.clock.flatDelay;
    this.clock[`${this.clock.player}DelayLeft`] += this.clock.presentDelay * timelineInfo.presentTimelines;
    this.clock[`${this.clock.player}DelayLeft`] += this.clock.activeDelay * timelineInfo.activeTimelines;
    this.clock[`${this.clock.player}DelayLeft`] += this.clock.timelineDelay * timelineInfo.timelines;
  }
  updateTime(time = null) {
    if(this.clock.running) {
      let currentTime = Date.now();
      if(typeof time === 'number') {
        currentTime = time;
      }
      let delta = currentTime - this.clock.lastUpdate;
      if(this.clock[`${this.clock.player}DelayLeft`] > 0) {
        this.clock[`${this.clock.player}DelayLeft`] -= delta;
        if(this.clock[`${this.clock.player}DelayLeft`] < 0) {
          delta = Math.abs(this.clock[`${this.clock.player}DelayLeft`]);
          this.clock[`${this.clock.player}DelayLeft`] = 0;
        }
        else {
          delta = 0;
        }
      }
      this.clock[`${this.clock.player}DurationLeft`] -= delta;
      if(this.clock[`${this.clock.player}DurationLeft`] < 0) {
        this.clock[`${this.clock.player}DurationLeft`] = 0;
        this.clock.running = false;
      }
      this.clock.lastUpdate = currentTime;
    }
  }
  switch(player = null, newBoard = {}, time = null) {
    this.updateTime(time);
    if(
      (player === 'white' && this.clock.player === 'black') ||
      (player === 'black' && this.clock.player === 'white')
    ) {
      this.clock.player = player;
    }
    else {
      this.clock.player = this.clock.player === 'white' ? 'black' : 'white';
    }
    if(this.clock.running) {
      this.addTime(newBoard);
    }
  }
  get format() {
    return formatFuncs.toString(this.clock);
  }
  get formats() {
    return formatFuncs.formats.map((v) => {
      return {
        name: v.name,
        shortName: v.key,
        format: v.value
      };
    });
  }
}

module.exports = ChessClock;