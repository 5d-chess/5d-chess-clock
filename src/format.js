const formats = [
  {
    key: 'bullet',
    name: 'Bullet',
    value: '5'
  },
  {
    key: 'blitz',
    name: 'Blitz',
    value: '10;delp5'
  },
  {
    key: 'rapid',
    name: 'Rapid',
    value: '20;delp10'
  },
  {
    key: 'standard',
    name: 'Standard',
    value: '40;delp20'
  },
  {
    key: 'tournament',
    name: 'Tournament',
    value: '80;delp40'
  },
  {
    key: 'daily',
    name: 'Daily',
    value: 'del86400'
  }
];

exports.formats = formats;

exports.fromString = (input) => {
  let res = {
    startingDuration: 0,
    flatIncrement: 0,
    presentIncrement: 0,
    activeIncrement: 0,
    timelineIncrement: 0,
    flatDelay: 0,
    presentDelay: 0,
    activeDelay: 0,
    timelineDelay: 0,
  };
  let terms = input.split(';');
  //Check if input is format
  for(let format of formats) {
    if(format.key === input || format.name === input) {
      terms = format.value.split(';');
    }
  }
  //Process terms into usable config object
  for(let term of terms) {
    if(term.match(/[\d\:]+/) && term.match(/[^\d\:]+/) === null) {
      if(term.includes(':')) {
        let match = term.match(/(\d+)\:(\d+)/);
        res.startingDuration = (Number(match[1]) * 60 * 1000) + (Number(match[2]) * 1000);
      }
      else {
        res.startingDuration = Number(term) * 60 * 1000;
      }
    }
    if(term.match(/inc\d+/)) {
      res.flatIncrement = Number(term.match(/inc(\d+)/)[1]) * 1000;
    }
    if(term.match(/incp\d+/)) {
      res.presentIncrement = Number(term.match(/incp(\d+)/)[1]) * 1000;
    }
    if(term.match(/inca\d+/)) {
      res.activeIncrement = Number(term.match(/inca(\d+)/)[1]) * 1000;
    }
    if(term.match(/inct\d+/)) {
      res.timelineIncrement = Number(term.match(/inct(\d+)/)[1]) * 1000;
    }
    if(term.match(/del\d+/)) {
      res.flatDelay = Number(term.match(/del(\d+)/)[1]) * 1000;
    }
    if(term.match(/delp\d+/)) {
      res.presentDelay = Number(term.match(/delp(\d+)/)[1]) * 1000;
    }
    if(term.match(/dela\d+/)) {
      res.activeDelay = Number(term.match(/dela(\d+)/)[1]) * 1000;
    }
    if(term.match(/delt\d+/)) {
      res.timelineDelay = Number(term.match(/delt(\d+)/)[1]) * 1000;
    }
  }
  return res;
}

exports.toString = (input) => {
  let res = '';
  /*
  {
    startingDuration: 0,
    flatIncrement: 0,
    presentIncrement: 0,
    activeIncrement: 0,
    timelineIncrement: 0,
    flatDelay: 0,
    presentDelay: 0,
    activeDelay: 0,
    timelineDelay: 0,
  };
  */
  //Base string
  let startingDurationMinutes = Math.floor(input.startingDuration / (60 * 1000));
  let startingDurationSeconds = Math.floor((input.startingDuration % (60 * 1000)) / 1000);
  if(startingDurationMinutes > 0 && startingDurationSeconds > 0) {
    res = `${startingDurationMinutes}:${startingDurationSeconds < 10 ? '0' + startingDurationSeconds : startingDurationSeconds}`;
  }
  else if(startingDurationMinutes <= 0 && startingDurationSeconds > 0) {
    res = `0:${startingDurationSeconds < 10 ? '0' + startingDurationSeconds : startingDurationSeconds}`;
  }
  else if(startingDurationMinutes > 0 && startingDurationSeconds <= 0) {
    res = `${startingDurationMinutes}`;
  }

  //Additional terms
  if(input.flatIncrement > 0) {
    res += `${res.length > 0 ? ';' : ''}inc${Math.floor(input.flatIncrement / 1000)}`;
  }
  if(input.presentIncrement > 0) {
    res += `${res.length > 0 ? ';' : ''}incp${Math.floor(input.presentIncrement / 1000)}`;
  }
  if(input.activeIncrement > 0) {
    res += `${res.length > 0 ? ';' : ''}inca${Math.floor(input.activeIncrement / 1000)}`;
  }
  if(input.timelineIncrement > 0) {
    res += `${res.length > 0 ? ';' : ''}inct${Math.floor(input.timelineIncrement / 1000)}`;
  }
  if(input.flatDelay > 0) {
    res += `${res.length > 0 ? ';' : ''}del${Math.floor(input.flatDelay / 1000)}`;
  }
  if(input.presentDelay > 0) {
    res += `${res.length > 0 ? ';' : ''}delp${Math.floor(input.presentDelay / 1000)}`;
  }
  if(input.activeDelay > 0) {
    res += `${res.length > 0 ? ';' : ''}dela${Math.floor(input.activeDelay / 1000)}`;
  }
  if(input.timelineDelay > 0) {
    res += `${res.length > 0 ? ';' : ''}delt${Math.floor(input.timelineDelay / 1000)}`;
  }

  //Check if format applies
  for(let format of formats) {
    if(format.value === res) {
      return format.name;
    }
  }
  return res;
}
