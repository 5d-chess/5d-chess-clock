exports.parse = (board) => {
  //Extract timeline info from 5d-chess-js json board
  let res = {
    timelines: 0,
    activeTimelines: 0,
    presentTimelines: 0
  };

  //Grab timeline number
  if(Array.isArray(board.timelines)) {
    res.timelines = board.timelines.length;
    for(let timeline of board.timelines) {
      //Get active timeline number
      if(timeline.active) {
        res.activeTimelines++;
      }
      //Get present timeline number
      if(timeline.present) {
        res.presentTimelines++;
      }
    }
  }

  return res;
}