const ChessClock = require('@local/index');
const deepequal = require('deep-equal');

test('Format Parsing', () => {
  let chessClock = new ChessClock();
  let format1 = '90;incp30';
  let format2 = chessClock.raw.formatFuncs.toString(chessClock.raw.formatFuncs.fromString(format1));
  expect(deepequal(format1, format2)).toBe(true);
  format1 = '0:30;inca10;delp2';
  format2 = chessClock.raw.formatFuncs.toString(chessClock.raw.formatFuncs.fromString(format1));
  expect(deepequal(format1, format2)).toBe(true);
  format1 = '10:30;inc10;delt2';
  format2 = chessClock.raw.formatFuncs.toString(chessClock.raw.formatFuncs.fromString(format1));
  expect(deepequal(format1, format2)).toBe(true);
});
