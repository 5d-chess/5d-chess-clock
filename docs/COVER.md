# 5D Chess Clock

> Javascript library used to manipulate 5d chess time controls. Includes definition for 5d chess time control notation.

[GitLab](https://gitlab.com/5d-chess/5d-chess-clock)
[Get Started](#overview)

![color](#000000)