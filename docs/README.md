# Overview

Javascript library used to manipulate 5d chess time controls. Includes definition for 5d chess time control notation.

# Installation

## Node.js

Simply install with this command `npm i 5d-chess-clock`

## In Browser

Include this tag in the HTML beofre invoking this library:
``` html
<script src="https://unpkg.com/5d-chess-clock/dist/5d-chess-clock.js"></script>
```

# Internal State

JSON Format for timing information (units are milliseconds)

  - `running` - Boolean indicating if the clock is running.
  - `lastUpdate` - Unix timestamp indicating the last time the clock was updated.
  - `player` - String enum indicating the current player in play (`"white"` or `"black"`).
  - `whiteDurationLeft` - Amount of time left for white to play.
  - `blackDurationLeft` - Amount of time left for black to play.
  - `whiteDelayLeft` - Amount of delay left for white to play.
  - `blackDelayLeft` - Amount of delay left for black to play.
  - `startingDuration` - Amount of time to give to both players at the start of the game.
  - `flatIncrement` - Amount of time to give to player when their turn starts.
  - `presentIncrement` - Amount of time to give to player when their turn starts (this scales per present timeline in play).
  - `activeIncrement` - Amount of time to give to player when their turn starts (this scales per active timeline in play).
  - `timelineIncrement` - Amount of time to give to player when their turn starts (this scales per timeline in play).
  - `flatDelay` - Amount of delay to give to player when their turn starts.
  - `presentDelay` - Amount of delay to give to player when their turn starts (this scales per present timeline in play).
  - `activeDelay` - Amount of delay to give to player when their turn starts (this scales per active timeline in play).
  - `timelineDelay` - Amount of delay to give to player when their turn starts (this scales per timeline in play).

# Format Notation

## Base String

If in dividing the starting duration by minutes and seconds are left as a remainder, the base string should be formatted as `m:ss`.

If no seconds are left as a remainder, the base string should be just the starting duration in minutes.

*Note: The smallest subdivision is only seconds, fractions of seconds are not acceptable (i.e. `0:00.5` is not valid)*

## Additional Terms

Available additional terms (deliminate with `;`)

 - flat increment - `inc<flat increment (secs)>`
 - present increment - `incp<present increment (secs)>`
 - active increment - `inca<present increment (secs)>`
 - timeline increment - `inct<present increment (secs)>`
 - flat delay - `del<flat delay (secs)>`
 - present delay - `delp<present delay (secs)>`
 - active delay - `dela<present delay (secs)>`
 - timeline delay - `delt<present delay (secs)>`

## Examples

 - `90;incp30` - 90 minute starting duration with 30 second increment per present timeline.
 - `0:30;inca10;delp2` - 30 second starting duration with 10 second increment (per active timeline) and 2 second delay (per present timeline).

# Supported Timing Formats

For the format string to indicating standardized time controls, these are the following values:

  - Bullet - Starting duration of 5 minutes (the string literal `"bullet"` is used internally).
  - Blitz - Starting duration of 10 minutes with 5 second delay for each present timeline (the string literal `"blitz"` is used internally).
  - Rapid - Starting duration of 20 minutes with 10 second delay for each present timeline (the string literal `"rapid"` is used internally).
  - Standard - Starting duration of 40 minutes with 20 second delay for each present timeline (the string literal `"standard"` is used internally).
  - Tournament - Starting duration of 80 minutes with 40 second delay for each present timeline (the string literal `"tournament"` is used internally).
  - Daily - Starting duration of 0 minutes with 24 hour delay (the string literal `"daily"` is used internally).

# API

## Constructor

### ChessClock([format])

Creates a new instance of the `ChessClock` class.

  - format - *[Optional]* Timing format information (or internal state) to configure the clock. Can be string using [format notation](?id=format-notation) or a [standardized format](?id=supported-timing-formats). Can also be a JSON object or string representing the [internal state](?id=internal-state).
  - **Return** - A new `ChessClock` instance.

## Fields

These fields are implemented as a getter function. If getter functions are unsupported on the platform, call these fields as a function instead (example .format becomes .format()).

### .format

  - **Return** - String of the timing format in use using [format notation](?id=format-notation). If the timing format matches a [standardized format](?id=supported-timing-formats), it will return that instead.

### .formats

  - **Return** - Array of objects indicating supported formats. Objects follow the format of `{ name: <full format name>, shortName: <internal format name>, value: <format notation> }`. Any of the fields can be used for `format` field.

## Functions

### .import(format)

Imports timing format information (or internal state) to use as the clock configuration. Also resets the clock.

  - format - Timing format information (or internal state) to configure the clock. Can be string using [format notation](?id=format-notation) or a [standardized format](?id=supported-timing-formats). Can also be a JSON object or string representing the [internal state](?id=internal-state).
  - **Return** - Nothing.

### .copy([timeStamp])

Create a new instance of the `ChessClock` class with the exact same [internal state](?id=internal-state). Will calculate clock changes (right before copy to ensure equality).

  - timeStamp - *[Optional]* Number representing the Unix timestamp (in milliseconds) used to calculate changes in the [internal state](?id=internal-state) of the clock. Uses `Date.now()` if this parameter is not passed.
  - **Return** - New `ChessClock` class instance.

### .state([timeStamp])

Get the [internal state](?id=internal-state) of this instance of the `ChessClock` class. Used internally by `.copy()`, this is used for serializable copy (for example, copying state from web worker to main thread). Will calculate clock changes.

Use this to get the clock's [internal state](?id=internal-state) for display.

  - timeStamp - *[Optional]* Number representing the Unix timestamp (in milliseconds) used to calculate changes in the [internal state](?id=internal-state) of the clock. Uses `Date.now()` if this parameter is not passed.
  - **Return** - Object (JSON compatible) representing the clock's [internal state](?id=internal-state).

### .state(state, [timeStamp])

Set the [internal state](?id=internal-state) of this instance of the `ChessClock` class. Used internally by `.copy()`, this is used for serializable copy (for example, copying state from web worker to main thread). Will calculate clock changes.

  - state - Object (JSON compatible) representing the clock's [internal state](?id=internal-state).
  - timeStamp - *[Optional]* Number representing the Unix timestamp (in milliseconds) used to calculate changes in the [internal state](?id=internal-state) of the clock. Uses `Date.now()` if this parameter is not passed.
  - **Return** - Nothing.

### .reset([format])

Resets the clock. Reconfiguring the clock using [format notation](?id=format-notation) or a [standardized format](?id=supported-timing-formats) is available.

  - format - *[Optional]* Timing format information (or internal state) to configure the clock. Can be string using [format notation](?id=format-notation) or a [standardized format](?id=supported-timing-formats).
  - **Return** - Nothing.

### .start([board, timeStamp])

Start the clock. Will calculate clock changes.

  - board - *[Optional]* `Board` object representing the starting board (https://5d-chess.gitlab.io/5d-chess-js/#/?id=board-1). Used to calculate timeline based increment and delay.
  - timeStamp - *[Optional]* Number representing the Unix timestamp (in milliseconds) used to calculate changes in the [internal state](?id=internal-state) of the clock. Uses `Date.now()` if this parameter is not passed.
  - **Return** - Nothing.

### .stop([timeStamp])

Stop / Pause the clock (does not reset the clock). Will calculate clock changes.

  - timeStamp - *[Optional]* Number representing the Unix timestamp (in milliseconds) used to calculate changes in the [internal state](?id=internal-state) of the clock. Uses `Date.now()` if this parameter is not passed.
  - **Return** - Nothing.

### .switch([player, board, timeStamp])

Switch the current player. Will calculate clock changes.

  - player - *[Optional]* String enum for the player to switch to (`"white"` or `"black"`).
  - board - *[Optional]* `Board` object representing the new board (https://5d-chess.gitlab.io/5d-chess-js/#/?id=board-1). Used to calculate timeline based increment and delay.
  - timeStamp - *[Optional]* Number representing the Unix timestamp (in milliseconds) used to calculate changes in the [internal state](?id=internal-state) of the clock. Uses `Date.now()` if this parameter is not passed.
  - **Return** - Nothing.

# FAQ

## Is it any good?

Yes (maybe).

## Why is this on GitLab instead of GitHub?

I made the switch from GitHub to GitLab mid 2019 when I was starting a new long term project called KSS. Back then, GitHub did not have many of the features it does now, such as integrated CI/CD and more. GitLab was the superior product in almost every way. Furthermore, as a believer in the open source, it seem ironic that open source software would be hosted on closed source platforms. With GitLab being open source, I can be sure that if GitLab.org crumbles, I can still maintain the overall project structure via GitLab instances. This allows me to preserve the Git repo itself, but also the issues, labels, rules, pipelines, etc. that are fundamental to a project. With GitHub, developers do not have this guarantee and they also do not have full control over their project structure.

For a (biased, but not untrue) comparison, visit this link [here](https://about.gitlab.com/devops-tools/github/decision-kit.html)

# Copyright

All source code is released under AGPL v3.0 (license can be found under the LICENSE file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.
