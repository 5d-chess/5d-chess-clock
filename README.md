# 5D Chess Clock

[![Pipeline Status](https://gitlab.com/5d-chess/5d-chess-clock/badges/master/pipeline.svg)](https://gitlab.com/%{project_path}/-/commits/master)
[![NPM version](https://img.shields.io/npm/v/5d-chess-clock.svg)](https://www.npmjs.com/package/5d-chess-clock)

Javascript library used to manipulate 5d chess time controls. Includes definition for 5d chess time control notation.

## Documentation

Documentation is available [here](https://5d-chess.gitlab.io/5d-chess-clock)!

To run local copy, use the command `npm run docs-start`.

## Copyright

All source code is released under AGPL v3.0 (license can be found under the LICENSE file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA v3.0.
